#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP_RUNNER(BitwiseOperator)
{
  RUN_TEST_CASE(BitwiseOperator, Func_and_ShouldReturnZeroIfOneOfArgumentZero);
  RUN_TEST_CASE(BitwiseOperator, Func_and_ShouldReturnRightValueOnModdleRange);
  RUN_TEST_CASE(BitwiseOperator, Func_and_ShouldReturnRightValueOnMinRange);
  RUN_TEST_CASE(BitwiseOperator, Func_and_ShouldReturnRightValueOnMaxRange);
}