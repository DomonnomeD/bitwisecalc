#include "unity_fixture.h"

static void RunAllTests(void)
{
  RUN_TEST_GROUP(BitwiseOperator);
}

int32_t main(int32_t argc, const char * argv[])
{
  return UnityMain(argc, argv, RunAllTests);
}
