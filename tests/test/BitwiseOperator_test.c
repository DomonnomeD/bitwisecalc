#include "BitwiseOperator.h"
#include "unity.h"
#include "unity_fixture.h"

TEST_GROUP(BitwiseOperator);

TEST_SETUP(BitwiseOperator){}

TEST_TEAR_DOWN(BitwiseOperator){}

TEST(BitwiseOperator, Func_and_ShouldReturnZeroIfOneOfArgumentZero){
  TEST_ASSERT_EQUAL(0, and(1,0));
  TEST_ASSERT_EQUAL(0, and(11,0));
  TEST_ASSERT_EQUAL(0, and(111,0));
  TEST_ASSERT_EQUAL(0, and(1111,0));
  TEST_ASSERT_EQUAL(0, and(0,1));
  TEST_ASSERT_EQUAL(0, and(0,11));
  TEST_ASSERT_EQUAL(0, and(0,111));
  TEST_ASSERT_EQUAL(0, and(0,1111));
}

TEST(BitwiseOperator, Func_and_ShouldReturnRightValueOnModdleRange){
  TEST_ASSERT_EQUAL(16, and(16,24));
  TEST_ASSERT_EQUAL(0, and(100,24));
  TEST_ASSERT_EQUAL(12461, and(45245,425455));
  TEST_ASSERT_EQUAL(128, and(4225,25578));
  TEST_ASSERT_EQUAL(25578, and(4294967295,25578));
  TEST_ASSERT_EQUAL(4194309, and(2589624581,8594198541));
}

TEST(BitwiseOperator, Func_and_ShouldReturnRightValueOnMinRange){
  TEST_ASSERT_EQUAL(0, and(0,0));
  TEST_ASSERT_EQUAL(0, and(1,0));
  TEST_ASSERT_EQUAL(0, and(0,1));
  TEST_ASSERT_EQUAL(1, and(1,1));
}

TEST(BitwiseOperator, Func_and_ShouldReturnRightValueOnMaxRange){
  TEST_ASSERT_EQUAL(4294967295, and(4294967295,4294967295));
  TEST_ASSERT_EQUAL(0, and(4294967295,0));
  TEST_ASSERT_EQUAL(0, and(0,4294967295));
  TEST_ASSERT_EQUAL(1, and(4294967295,1));
  TEST_ASSERT_EQUAL(1, and(1,4294967295));
  TEST_ASSERT_EQUAL(4294967294, and(4294967294,4294967295));
}