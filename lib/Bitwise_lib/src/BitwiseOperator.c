#include "BitwiseOperator.h"



ADDAPI uint32_t and(uint32_t a,uint32_t b){
    return (a&b);
}

ADDAPI uint32_t or(uint32_t a,uint32_t b){
    return (a|b);
}

ADDAPI uint32_t xor(uint32_t a,uint32_t b){
    return (a^b);
}

ADDAPI uint32_t shift_left(uint32_t a,uint32_t b){
    return (a<<b);
}

ADDAPI uint32_t shift_right(uint32_t a,uint32_t b){
    return (a>>b);
}

ADDAPI uint32_t not(uint32_t a){
    return (~a);
}