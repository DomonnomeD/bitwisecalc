#ifndef BITWISE_OPERATOR_H
#define BITWISE_OPERATOR_H

#ifdef ADD_EXPORTS
  #define ADDAPI __declspec(dllexport)
#else
  #define ADDAPI __declspec(dllimport)
#endif

#include <stdint.h>

ADDAPI uint32_t and(uint32_t a,uint32_t b);
ADDAPI uint32_t or(uint32_t a,uint32_t b);
ADDAPI uint32_t xor(uint32_t a,uint32_t b);
ADDAPI uint32_t shift_left(uint32_t a,uint32_t b);
ADDAPI uint32_t shift_right(uint32_t a,uint32_t b);
ADDAPI uint32_t not(uint32_t a);

#endif