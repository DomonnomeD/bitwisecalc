#include "main.h"

int32_t main(int32_t argc, char *argv[]) {
    if(!is_valid_cmd_input_data_type(argc,argv)){
        printf("Bad command-line arguments");
        exit(EXIT_FAILURE);
    };

    if(THREE_INPUTS<argc){
        printf("To many inputs");
        exit(EXIT_FAILURE);
    }

    uint32_t operand[2] = {0};
    for(uint32_t i=INDEX_FIRST_OPERAND; i<argc; i++){
        const uint32_t base = 10;
        // printf("%d %s\n",i,argv[i]);
        operand[i-INDEX_FIRST_OPERAND] = \
            strtoul(argv[i], NULL, base);
        if(ERANGE==errno){
            printf("Unable to interpret input, to big number");
            exit(EXIT_FAILURE);
        }
    }

    uint32_t answer = 0;
    switch(argc)
    {
        case TWO_INPUTS:
            if(0==strcasecmp(argv[INDEX_OPERATOR],OPERATOR_NOT)){
                answer=not(operand[0]);
            } else{
                printf("Bad operator");
                exit(EXIT_FAILURE);
            }
            break;
        case THREE_INPUTS:
            if(0==strcasecmp(argv[INDEX_OPERATOR],OPERATOR_AND)){
                answer=and(operand[0],operand[1]);
            } else if(0==strcasecmp(argv[INDEX_OPERATOR],OPERATOR_OR)){
                answer=or(operand[0],operand[1]);
            } else if(0==strcasecmp(argv[INDEX_OPERATOR],OPERATOR_XOR)){
                answer=xor(operand[0],operand[1]);
            } else if(0==strcasecmp(argv[INDEX_OPERATOR],OPERATOR_SHIFT_LEFT)){
                answer=shift_left(operand[0],operand[1]);
            } else if(0==strcasecmp(argv[INDEX_OPERATOR],OPERATOR_SHIFT_RIGHT)){
                answer=shift_right(operand[0],operand[1]);
            } else{
                printf("Bad operator");
                exit(EXIT_FAILURE);
            }
            break;
        default:
            printf("Bad command-line arguments");
            exit(EXIT_FAILURE);
    }

    printf("%lu",answer);

    exit(EXIT_SUCCESS);
}

static bool is_valid_cmd_input_data_type(int32_t argc, char *argv[]){
    bool all_good = true;
    if(INDEX_FIRST_OPERAND+1==argc || INDEX_SECOND_OPERAND+1==argc){
        if(is_num(argv[INDEX_OPERATOR]))
            all_good = false;
        for(int32_t i=INDEX_FIRST_OPERAND; i<argc; i++)
            if(!is_num(argv[i]))
                all_good = false;        
    }
    return all_good;
}