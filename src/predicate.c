#include "predicate.h"

inline bool is_num(const char *num_or_not)
{
    for(int32_t i=0; i<strlen(num_or_not); i++)
        if (!isdigit(num_or_not[i]))
            return false;
    return true;
}