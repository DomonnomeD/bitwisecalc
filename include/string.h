#ifndef STRINGS_H
#define STRINGS_H

#include <stdint.h>

/**
* \brief Convert string to uppercase
* \param[in] string pointer
* @warning input must contain '\0'
*/
void to_upper(char *ch);

#endif