#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>

#include "predicate.h"
#include "string.h"
#include "BitwiseOperator.h"

#define TWO_INPUTS              3   //name.exe operator operand1 
#define THREE_INPUTS            4   //name.exe operator operand1 operand2
#define INDEX_OPERATOR          1
#define INDEX_FIRST_OPERAND     2
#define INDEX_SECOND_OPERAND    3

//have two operands
#define OPERATOR_AND            "AND"
#define OPERATOR_OR             "OR"
#define OPERATOR_XOR            "XOR"
#define OPERATOR_SHIFT_LEFT     "SHIFT_LEFT"
#define OPERATOR_SHIFT_RIGHT    "SHIFT_RIGHT"
//have one operand
#define OPERATOR_NOT            "NOT"

//private
static bool is_valid_cmd_input_data_type(int32_t argc, char *argv[]);

#endif