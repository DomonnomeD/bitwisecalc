#ifndef PREDICATE_H
#define PREDICATE_H

#include <stdbool.h>
#include <stdint.h>

/**
* \brief checking if char representing an integer (0-9)
* \param[in] pointer to string
* \return true - number, false - not number
* @warning input must contain '\0'
*/
extern inline bool is_num(const char *num_or_not);

#endif