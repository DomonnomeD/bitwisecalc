The Bitwise Calculator is used to perform bitwise AND, bitwise OR, bitwise XOR (bitwise exclusive or) operations on two integers (32 bit). It is also possible to perform bit shift operations.

#### Parameters
BitwiseCalc.exe operator operand1 operand2  
*operand2 not using, if operator "not"*

operator:  
 - and  
 - or  
 - xor  
 - shift_left  
 - shift_right  
 - not  

#### Egzample
```
BitwiseCalc.exe not 1             -> 4294967294
BitwiseCalc.exe not 4294967294    -> 1
BitwiseCalc.exe and 1 0           -> 0
```

#### Installation
```
git clone
```
if already have wget:
```
tools/win/build_tool/make init
```
if not, and dont want install:  
 - [download](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win64/Personal%20Builds/mingw-builds/8.1.0/threads-posix/seh/x86_64-8.1.0-release-posix-seh-rt_v6-rev0.7z/download) archive  
 - exctract into *tools\win*    
 -- final tree *tools\win\mingw64*    

#### Compiling
1) compile library:
```
tools/win/build_tool/make lib
```

2) compile program:
```
tools/win/build_tool/make
```

for more information:
```
tools/win/build_tool/make help
```