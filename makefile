ifeq ($(origin .RECIPEPREFIX), undefined)
  $(error This Make does not support .RECIPEPREFIX. Please use GNU Make 4.0 or later)
endif
.RECIPEPREFIX = >

############################
# TOOLCHAIN
############################
TOOLCHAIN_LINK = "https://sourceforge.net/projects/mingw-w64/files/Toolchains targetting Win64/Personal Builds/mingw-builds/8.1.0/threads-win32/seh/x86_64-8.1.0-release-win32-seh-rt_v6-rev0.7z"
TOOLCHAIN = $(RP_TOOLS_WIN)/x86_64-8.1.0-release-win32-seh-rt_v6-rev0.7z
############################
# EXE
############################
CC_GCC = ${FP_CUR_DIR}/tools/win/mingw64/bin/gcc.exe
MAKE = ${FP_CUR_DIR}/tools/win/build_tool/make.exe
RM =  ${FP_CUR_DIR}/tools/win/linux_tools/rm.exe
ECHO = ${FP_CUR_DIR}/tools/win/linux_tools/echo.exe
TEST = ${FP_CUR_DIR}/tools/win/linux_tools/test.exe
MKDIR =  ${FP_CUR_DIR}/tools/win/linux_tools/mkdir.exe
7ZA =  ${FP_CUR_DIR}/tools/win/7z/7za.exe

############################
# EXE from WIN side
############################
DOXYGEN = 
#SHELL = C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
SHELL = C:\windows\system32\cmd.exe
WGET =  wget
CLS = cls
COPY = copy

############################
# PATH: Relative Path (RP) / Full Path (FP)
############################
FP_CUR_DIR = $(CURDIR)
RP_CUR_DIR = .

############################
# Project structure Relative PATH
############################
RP_TOOLS_WIN = $(RP_CUR_DIR)/tools/win
RP_INCLUDE = $(RP_CUR_DIR)/include
RP_SRC = $(RP_CUR_DIR)/src
RP_DOC = $(RP_CUR_DIR)/doc
RP_BUILD = $(RP_CUR_DIR)/build
RP_TESTS = $(RP_CUR_DIR)/tests
RP_BITWISE_LIB = $(RP_CUR_DIR)/lib/Bitwise_lib
RP_BITWISE_LIB_DLL = $(RP_BITWISE_LIB)/lib
RP_BITWISE_LIB_INCLUDE = $(RP_CUR_DIR)/lib/Bitwise_lib/include

############################
# Project build path & target
############################
TARGET = $(RP_BUILD)/BitwiseCalc.exe
ifeq "$(findstring debug, $(MAKECMDGOALS))" "debug"
  RP_BUILD := $(RP_CUR_DIR)/build/debug
  DEBUG_FLAG = -g
else
  RP_BUILD := $(RP_CUR_DIR)/build/release
  DEBUG_FLAG = 
endif

############################
# Add if appeared new directory with .c files
############################
_C = $(wildcard $(RP_SRC)/*.c)

############################
# Add if appeared new directory with .h files
############################
INCLUDES = -I$(RP_INCLUDE)
INCLUDES += -I$(RP_BITWISE_LIB_INCLUDE)

_H = $(wildcard $(RP_INCLUDE)/*.h)
_H += $(wildcard $(RP_BITWISE_LIB_INCLUDE)/*.h)

############################
# Add if appeared new directory with .dll files
############################
LIB = -L$(RP_BITWISE_LIB_DLL)
LIB += -llibBIT
_LIB = $(RP_BITWISE_LIB_DLL)/libBIT.dll
############################
# Generated .o .d files with path
############################
_S = $(subst .//,,$(patsubst %.c,$(RP_BUILD)/%.s,$(_C)))
_O = $(subst .//,,$(patsubst %.c,$(RP_BUILD)/%.o,$(_C)))
_D = $(subst .//,,$(patsubst %.c,$(RP_BUILD)/%.d,$(_C)))

############################
# Compilator param
############################
C_PARAM_COMPILE = -std=c99
C_PARAM_ARCH = -march=x86-64
C_ASM = -fverbose-asm
########################################################
########################################################
########################################################

.PHONY: ALL
ALL : $(TARGET)
> $(COPY) $(subst /,\,$(_LIB))  $(subst /,\,$(RP_BUILD))

.PHONY: init
init : 
> $(WGET) $(TOOLCHAIN_LINK) -P $(RP_TOOLS_WIN)
> $(7ZA) x $(TOOLCHAIN) -o$(RP_TOOLS_WIN)
> $(RM) -r $(TOOLCHAIN)

.PHONY: clean_screen
clean_screen :
> $(CLS)

.PHONY: test
test :
> $(MAKE) -C $(RP_TESTS)

.PHONY: test_clean
test_clean :
> $(MAKE) -C $(RP_TESTS) clean

.PHONY: clean
clean :
> $(RM) -r $(RP_BUILD)

.PHONY: help
help :
> $(ECHO) -e "Generate assemble: \t\t make asm"
> $(ECHO) -e "Show all build info: \t\t make Silent_OFF=1"
> $(ECHO) -e "Make release: \t\t\t make"
> $(ECHO) -e "Make release: \t\t\t make release"
> $(ECHO) -e "Make debug: \t\t\t make debug"
> $(ECHO) -e "Make test: \t\t\t make test"
> $(ECHO) -e "Make dependencies: \t\t make gen_dep"
> $(ECHO) -e "Make library: \t\t\t make lib"
> $(ECHO) -e "Make test build files: \t\t make test_clean"
> $(ECHO) -e "Clean build files: \t\t make clean"
> $(ECHO) -e "Clean library build files: \t make lib_clean"

.PHONY: lib
lib : 
> $(MAKE) -C $(RP_BITWISE_LIB)

.PHONY: lib_clean
lib_clean : 
> $(MAKE) -C $(RP_BITWISE_LIB) clean

.PHONY: asm
asm : $(_S)
.PHONY: debug
debug : ALL
.PHONY: release
release : ALL
.PHONY: gen_dep
gen_dep: $(_D)


$(RP_BUILD)/%.s : %.c
> $(ECHO) ______________________________
> $(ECHO) assemble FILE: [$(notdir $@)]
> $(ECHO) FROM: [$(notdir $^)]
> $(MKDIR) -p $(@D)
> $(CC_GCC) $(INCLUDES) $(C_ASM) -S -o $@ $<

$(RP_BUILD)/%.o : %.c
> $(ECHO) ______________________________
> $(ECHO) compile FILE: [$(notdir $@)]
> $(ECHO) FROM: [$(notdir $^)]
> $(MKDIR) -p $(@D)
> $(CC_GCC) $(INCLUDES) $(C_PARAM_ARCH) $(C_PARAM_COMPILE) $(DEBUG_FLAG) -c $< -o $@

$(TARGET) : $(_O)
> $(ECHO) ______________________________
> $(ECHO) $@
> $(ECHO) linking [$(subst $(RP_CUR_DIR)/,./,$@)] 
> $(ECHO) FROM: [$(notdir $^)]
> $(CC_GCC) $(C_PARAM_ARCH) $(LIB) -o $@ $^
> IF EXIST $(TARGET) ($(ECHO) SUCCESS) ELSE ($(ECHO) ERROR)

$(RP_BUILD)/%.d : $(RP_CUR_DIR)/%.c
> $(ECHO) ______________________________
> $(ECHO) generate dependencies FILE: [$(notdir $@)]
> $(ECHO) in PATH: [$(dir $@)]
> $(MKDIR) -p $(@D)
> $(CC_GCC) $(INCLUDES) -MM -MT "$(patsubst %.d,%.o,$@)" -o $@ -c $<

-include $(_D)

$(Silent_OFF).SILENT: